package com.example.springretry.service;

import org.springframework.retry.annotation.Backoff;
import org.springframework.retry.annotation.Recover;
import org.springframework.retry.annotation.Retryable;
import org.springframework.stereotype.Service;

@Service
public class RetryService {
    @Retryable(value = Exception.class,maxAttempts = 2, backoff = @Backoff(delay = 10000))
    public void retry() throws Exception {
        System.out.println("This is retry");
        throw new Exception();
    }
    @Recover
    void recover(Exception e, String sql){
        System.out.println("This is recover method");
    }
}
