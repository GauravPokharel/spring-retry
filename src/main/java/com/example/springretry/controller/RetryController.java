package com.example.springretry.controller;

import com.example.springretry.service.RetryService;
import lombok.AllArgsConstructor;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@AllArgsConstructor
@RequestMapping("")
public class RetryController {
    private final RetryService retryService;

    @GetMapping("")
    public String retry() throws Exception{
        retryService.retry();
        return "This is retry controller";
    }
}
